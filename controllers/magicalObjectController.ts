import { Prisma, PrismaClient, MagicalObject } from "@prisma/client";
import { Request, Response } from "express";

import { response, listResponse } from "@/utils/response.utils";

export default {
  async create(req: Request, res: Response): Promise<Response> {
    const prisma = new PrismaClient();

    const result: MagicalObject = await prisma.magicalObject.create({
      data: {
        name: req.body.name
      }
    });

    await prisma.$disconnect();
    return res
      .status(201)
      .json(response("Magical Object successfully created", result));
  },
  async readAll(req: Request, res: Response): Promise<Response> {
    const prisma = new PrismaClient();

    const page: number = Number.parseInt(req.query.page.toString());
    const pageSize: number = Number.parseInt(req.query.pageSize.toString());

    const count = await prisma.magicalObject.count();
    const results = await prisma.magicalObject.findMany({
      skip: (page - 1) * pageSize,
      take: pageSize
    });

    await prisma.$disconnect();
    return res
      .status(200)
      .json(
        listResponse(
          "Magical Objects successfully fetched",
          count,
          page,
          pageSize,
          results
        )
      );
  }
};

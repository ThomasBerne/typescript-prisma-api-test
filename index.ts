// #region Import Required External Modules

import * as express from "express";
import * as logger from "morgan";

// #endregion
// #region App Variables

const app = express();
const port = 3000;

app.set("port", port);

// #endregion
// #region App Configuration

// Initialize Morgan
app.use(logger("dev"));

// Initialize body-parser
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// #endregion
// #region API Initialization

import route from "@/routes";
route(app);

// #endregion
// #region Server Activation

app.listen(port, () => {
  console.log("\nServer started on http://localhost:" + port);
});

// #endregion

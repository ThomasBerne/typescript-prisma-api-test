import { Express, Request, Response } from "express";

import magicalObjectRoutes from "./magicalObjectRoutes";

export default (app: Express): void => {
  magicalObjectRoutes(app);
  app.get("*", (req: Request, res: Response) => {
    res.status(200).send({
      message: "Welcome to the beginning of nothingness"
    });
  });
};

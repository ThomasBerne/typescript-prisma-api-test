import { Express } from "express";

import { magicalObjectController } from "@/controllers";

export default (app: Express): void => {
  app.post("/api/magicalObject", magicalObjectController.create);
  app.get("/api/magicalObject", magicalObjectController.readAll);
};

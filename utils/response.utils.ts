export function response(message: string, data: object): object {
  return { message, data };
}

export function listResponse(
  message: string,
  count: number,
  page: number,
  pageSize: number,
  data: object
): object {
  return {
    message,
    count,
    previous: page > 1 ? page - 1 : null,
    next: count > page * pageSize ? page + 1 : null,
    data
  };
}
